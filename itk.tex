\chapter{Integrators~Toolkit}\label{chap:itk}

The backend of the \gls{edp} is programmed using Java.
In order to have full flexibility, every \gls{jvm} code can be
included at runtime%
\footnote{
For simplicity, this document only considers Java.
} using the \gls{itk}.
One must have considerable knowledge on a \gls{jvm} language for implementing
an \gls{itk} extension.

\medskip

For this purpose, the repository \texttt{edp-itk}\cite{edp-itk-rep} is provided.
This repository contains abstract classes and interfaces.
Custom code can be created by implementing subclasses of the
abstract classes and interfaces.
The implementation must be complied to a \gls{jar} and added to the 
\texttt{CLASSPATH}%
\footnote{In this document, \texttt{Bash} syntax is used.}
\begin{lstlisting}
export CLASSPATH=$CLASSPATH:/full/path/to/jar.jar
\end{lstlisting}

\section{Setup}

We recommend to use \textit{Apache~Maven} when implementing an
\gls{itk} module.
Clone the repository \texttt{edp-itk}\cite{edp-itk-rep} and
execute 
\begin{lstlisting}
mvn install
\end{lstlisting}
The documentation can be generated with
\begin{lstlisting}
mvn javadoc:javadoc
\end{lstlisting}
an accessed in the sub directory \texttt{./target/site/apidocs/index} afterwards.

\bigskip

Add the \texttt{edp-itk} as Maven dependency to your \gls{itk} module
(\texttt{pom.xml} in listing~\ref{lst:pom-xml} in \cite{edp-tech-cds}).

\begin{lstlisting}[ style=xml
                  , caption=Add \gls{itk} to \texttt{pom.xml} 
                  , label={lst:pom-xml}
                  , frame=l
                  , numbers=left
                  , xleftmargin=.35\textwidth
                  , basicstyle=\tt\small
                  ]
<dependencies>
      .
  <dependency>
    <groupId>edlab.eda</groupId>
    <artifactId>edp.itk</artifactId>
    <version>draft</version>
  </dependency>
      .
</dependencies>
\end{lstlisting}

When you finished your technology integration, create a \gls{jar} 
of your code with
\begin{lstlisting}
mvn package
\end{lstlisting}

\newpage

\section{Technology}

A technology \cite{edp-tech-man} can be customized by creating a subclass
of \texttt{TechnologyIntegration}.
A corresponding template is shown in listing~\ref{lst:tech-int}. 
The full path to the class\\
(\texttt{com.company.itk.cds.CustomTechnologyIntegration} in
listing~\ref{lst:tech-int}) must be provided to the attribute \texttt{itk} in
the technology file~\cite{edp-tech-man}.
An example is provided in the
repository \texttt{edp-pdk-cds}~\cite{edp-tech-cds}.

\begin{lstlisting}[style=java,caption=Extension of Technology,label={lst:tech-int}]
package edlab.eda.edp.itk.custom;

import edlab.eda.edp.itk.device.CallbackEngine;
import edlab.eda.edp.itk.nl.Formatter;
import edlab.eda.edp.itk.nl.NetlistingEngine;
import edlab.eda.edp.itk.technology.TechnologyIntegration;

public class CustomTechnologyIntegration extends TechnologyIntegration {

  @Override
  public boolean hasNetlistingEngine(final String simulator) {
    // TODO
  }

  @Override
  public NetlistingEngine getNetlistingEngine(final String simulator,
      final Formatter formatter) {
    // TODO
  }

  @Override
  public boolean hasCallbackEngine(final String libName,
      final String cellName) {
    // TODO
  }

  @Override
  public CallbackEngine getCallbackEngine(final String libName,
      final String cellName) {
    // TODO
  }
}
\end{lstlisting}

The \texttt{// TODO} placeholders must be filled with the \gls{pdk} specific 
implementations.

\begin{itemize}
  \item \textbf{hasNetlistingEngine} \\
    \texttt{public boolean \textbf{hasNetlistingEngine}(final String simulator)}

  \begin{itemize}
    \item[-] \textit{\textbf{Description}} \\
      Check if the \texttt{TechnologyIntegration} can return a
      \texttt{NetlistingEngine} for a simulator with a given name
    \item[-] \textit{\textbf{Parameters}}
    \begin{description}
      \item[simulator] Name of simulator, e.g. \textit{"spectre"}
    \end{description}
    \item[-] \textit{\textbf{Returns}}
    \begin{description}
      \item[]\texttt{true} when the \texttt{TechnologyIntegration} can return
        a \texttt{NetlistingEngine} for the simulator, \texttt{false}
        otherwise.
    \end{description}
  \end{itemize}

  \item \textbf{getNetlistingEngine} \\
    \texttt{public NetlistingEngine \textbf{getNetlistingEngine}(final String simulator,\\
       final Formatter formatter)}

  \begin{itemize}
    \item[-] \textit{\textbf{Description}} \\
      Get a \texttt{NetlistingEngine} for the simulator
    \item[-] \textit{\textbf{Parameters}}
    \begin{description}
      \item[simulator] Name of simulator, e.g. \textit{"spectre"}
      \item[formatter] Netlisting formatter
    \end{description}
    \item[-] \textit{\textbf{Returns}}
    \begin{description}
      \item[]netlisting engine
    \end{description}
  \end{itemize}

  \item \textbf{hasCallbackEngine} \\
    \texttt{public boolean \textbf{hasCallbackEngine}(final String libName, final String cellName)}
  \begin{itemize}
    \item[-] \textit{\textbf{Description}} \\
      Identify if a primitive has a callback engine
    \item[-] \textit{\textbf{Parameters}}
    \begin{description}
      \item[libName] Library name
      \item[cellName] Cell name    
    \end{description}
    \item[-] \textit{\textbf{Returns}}
    \begin{description}
      \item[]\texttt{true} when the device has a \texttt{CallbackEngine},
        \texttt{false} otherwise
    \end{description}
  \end{itemize}

  \item \textbf{getCallbackEngine} \\
    \texttt{public CallbackEngine \textbf{getCallbackEngine}(final String libName, final String cellName)}
  \begin{itemize}
    \item[-] \textit{\textbf{Description}} \\
      Get a \texttt{CallbackEngine} of a device
    \item[-] \textit{\textbf{Parameters}}
    \begin{description}
      \item[libName] Library name
      \item[cellName] Cell name    
    \end{description}
    \item[-] \textit{\textbf{Returns}}
    \begin{description}
      \item[] callback engine when available, \texttt{null} otherwise
    \end{description}
  \end{itemize}
\end{itemize}

\subsection{Nestling~Engine}

By default, the individual devices are netlisted in Virtuoso, which is slow.
Netlisting can be speed up by adding a custom \texttt{NetlistingEngine}
via \gls{itk}.

\begin{lstlisting}[style=java,caption=Extension of Netlisting Engine,label={lst:tech-nl-engine}]
package edlab.eda.edp.itk.custom;

import edlab.eda.edp.itk.nl.Formatter;
import edlab.eda.edp.itk.nl.InstanceNetlistingHandle;
import edlab.eda.edp.itk.nl.NetlistingEngine;

public class CustomNetlistingEngine extends NetlistingEngine {

  public CustomNetlistingEngine(final Formatter formatter) {
    super(formatter);
  }

  @Override
  public boolean isDescribed(final InstanceNetlistingHandle handle) {
    // TODO
  }

  @Override
  public void netlist(final InstanceNetlistingHandle handle) {
    // TODO
  }
}
\end{lstlisting}

The \texttt{// TODO} placeholders must be filled with the \gls{pdk} specific 
implementations.

\begin{itemize}
  \item \textbf{isDescribed} \\
    \texttt{public boolean \textbf{isDescribed}(final InstanceNetlistingHandle handle)}

  \begin{itemize}
    \item[-] \textit{\textbf{Description}} \\
      Check if an engine is able to netlist an \texttt{InstanceNetlistingHandle}
    \item[-] \textit{\textbf{Parameters}}
    \begin{description}
      \item[handle] Handle to the instance
    \end{description}
    \item[-] \textit{\textbf{Returns}}
    \begin{description}
      \item[] \texttt{true} when the engine can netlist the
        \texttt{InstanceNetlistingHandle}, \texttt{false} otherwise
    \end{description}
  \end{itemize}

  \item \textbf{netlist} \\
    \texttt{public void \texttt{netlist}(final InstanceNetlistingHandle handle) }

  \begin{itemize}
    \item[-] \textit{\textbf{Description}} \\
      Netlist an \texttt{InstanceNetlistingHandle}
    \item[-] \textit{\textbf{Parameters}}
    \begin{description}
      \item[handle] Handle to the instance
    \end{description}
  \end{itemize}
\end{itemize}


\subsubsection{Formatter}

An instance which implements the interface \texttt{Formatter} provides
method how to append a netlisting statement to a netlist.
All methods are described below:

\begin{itemize}
  \item \textbf{isSubcircuitName} \\
    \texttt{boolean \textbf{isSubcircuitName}(String name)}

  \begin{itemize}
    \item[-] \textit{\textbf{Description}} \\
      Check if a subcircuit with a given name is already added to the netlist
    \item[-] \textit{\textbf{Parameters}}
    \begin{description}
      \item[name] Name to be checked
    \end{description}
    \item[-] \textit{\textbf{Returns}}
    \begin{description}
      \item[] \texttt{true} when is already a subcircuit name, 
        \texttt{false} otherwise
    \end{description}
  \end{itemize}

  \item \textbf{append} \\
    \texttt{Formatter \textbf{append}(InstanceNetlistingHandle handle, String statement)}

  \begin{itemize}
    \item[-] \textit{\textbf{Description}} \\
      Append a netlisting statement for a given instance
    \item[-] \textit{\textbf{Parameters}}
    \begin{description}
      \item[handle] Handle to the instance
      \item[statement] Netlisting statement to be added
    \end{description}
    \item[-] \textit{\textbf{Returns}}
    \begin{description}
      \item[] \texttt{this} when successful, \texttt{null}  otherwise
    \end{description}
  \end{itemize}

  \item \textbf{append} \\
    \texttt{Formatter \textbf{append}(InstanceNetlistingHandle handle, \\
         NetlistStatementBuilder builder)}

  \begin{itemize}
    \item[-] \textit{\textbf{Description}} \\
      Append a netlisting statement for a given instance
    \item[-] \textit{\textbf{Parameters}}
    \begin{description}
      \item[handle] Handle to the instance
      \item[builder] Builder
    \end{description}
    \item[-] \textit{\textbf{Returns}}
    \begin{description}
      \item[] \texttt{this} when successful, \texttt{null}  otherwise
    \end{description}
  \end{itemize}

  \item \textbf{append} \\
    \texttt{Formatter \textbf{append}(String name, String statement)}

  \begin{itemize}
    \item[-] \textit{\textbf{Description}} \\
      Append a named netlist statement
    \item[-] \textit{\textbf{Parameters}}
    \begin{description}
      \item[name] Name
      \item[statement] Netlisting statement to be added
    \end{description}
    \item[-] \textit{\textbf{Returns}}
    \begin{description}
      \item[] \texttt{this} when successful, \texttt{null}  otherwise
    \end{description}
  \end{itemize}

  \item \textbf{append} \\
    \texttt{Formatter \textbf{append}(String name, NetlistStatementBuilder builder)}

  \begin{itemize}
    \item[-] \textit{\textbf{Description}} \\
      Append a named netlist statement
    \item[-] \textit{\textbf{Parameters}}
    \begin{description}
      \item[handle] Name
      \item[builder] Builder
    \end{description}
    \item[-] \textit{\textbf{Returns}}
    \begin{description}
      \item[] \texttt{this} when successful, \texttt{null}  otherwise
    \end{description}
  \end{itemize}

  \item \textbf{append} \\
    \texttt{Formatter \textbf{append}(String name, NetlistStatementBuilder builder)}

  \begin{itemize}
    \item[-] \textit{\textbf{Description}} \\
      Append an unnamed netlist statement
    \item[-] \textit{\textbf{Parameters}}
    \begin{description}
      \item[name] Name
      \item[builder] Builder
    \end{description}
    \item[-] \textit{\textbf{Returns}}
    \begin{description}
      \item[] \texttt{this} when successful, \texttt{null}  otherwise
    \end{description}
  \end{itemize}

  \item \textbf{append} \\
    \texttt{Formatter \textbf{append}(String statement)}

  \begin{itemize}
    \item[-] \textit{\textbf{Description}} \\
      Append an unnamed netlist statement
    \item[-] \textit{\textbf{Parameters}}
    \begin{description}
      \item[name] Name
    \end{description}
    \item[-] \textit{\textbf{Returns}}
    \begin{description}
      \item[] \texttt{this} when successful, \texttt{null}  otherwise
    \end{description}
  \end{itemize}

\end{itemize}

\subsubsection{Netlist Statement Builder}

Get a netlist statement builder which takes care of maximal line length and
continuation characters.
All methods are described below:
\begin{itemize}
  \item \textbf{append} \\
    \texttt{NetlistStatementBuilder \textbf{append}(String token)}

  \begin{itemize}
    \item[-] \textit{\textbf{Description}} \\
      Append a new token to the statement
    \item[-] \textit{\textbf{Parameters}}
    \begin{description}
      \item[token] Token that is appended to the statement
    \end{description}
    \item[-] \textit{\textbf{Returns}}
    \begin{description}
      \item[] \texttt{this}
    \end{description}
  \end{itemize}

  \item \textbf{getLines} \\
    \texttt{String {[]} \textbf{getLines}()}

  \begin{itemize}
    \item[-] \textit{\textbf{Description}} \\
      Get all lines of the netlist statement
    \item[-] \textit{\textbf{Returns}}
    \begin{description}
      \item[] \texttt{lines}
    \end{description}
  \end{itemize}

\end{itemize}


\subsubsection{Instance Netlisting Handle}

An object of the class \texttt{InstanceNetlistingHandle}
provides all information how to netlist an instance.

The methods of the class are described in the enumeration below:

\begin{itemize}
  \item \textbf{getInstanceName} \\
    \texttt{public String \textbf{getInstanceName}()}

  \begin{itemize}
    \item[-] \textit{\textbf{Description}} \\
      Get the instance name
    \item[-] \textit{\textbf{Returns}}
    \begin{description}
      \item[] instance~name
    \end{description}
  \end{itemize}
  \item \textbf{getConnections} \\
    \texttt{public Map<String, String> \textbf{getConnections}()}

  \begin{itemize}
    \item[-] \textit{\textbf{Description}} \\
      Get the connections to the device
    \item[-] \textit{\textbf{Returns}}
    \begin{description}
      \item[] map of connections. The key corresponds to the terminal and the
       value to the attached net/signal.
    \end{description}
  \end{itemize}

  \item \textbf{getLibraryName} \\
    \texttt{public String \textbf{getLibraryName}()}
  \begin{itemize}
    \item[-] \textit{\textbf{Description}} \\
      Get library name of the primitive
    \item[-] \textit{\textbf{Returns}}
    \begin{description}
      \item[] library~name
    \end{description}
  \end{itemize}

  \item \textbf{getCellName} \\
    \texttt{public String \textbf{getCellName}()}
  \begin{itemize}
    \item[-] \textit{\textbf{Description}} \\
       Get cell name of the primitive
    \item[-] \textit{\textbf{Returns}}
    \begin{description}
      \item[] cell~name
    \end{description}
  \end{itemize}

  \item \textbf{inSubcircuit} \\
    \texttt{public boolean \textbf{inSubcircuit}()}
  \begin{itemize}
    \item[-] \textit{\textbf{Description}} \\
      Identify if the instance is defined in subcircuit
    \item[-] \textit{\textbf{Returns}}
    \begin{description}
      \item[] \texttt{true} when the instance is defined in a subcircuit,
        \texttt{false} otherwise
    \end{description}
  \end{itemize}

  \item \textbf{getParameters} \\
    \texttt{public Map<String, String> \textbf{getParameters}()}
  \begin{itemize}
    \item[-] \textit{\textbf{Description}} \\
      Get the parameters of the device
    \item[-] \textit{\textbf{Returns}}
    \begin{description}
      \item[] map of parameters. The key corresponds to the parameter name and
        the value to the parameter value. When a parameter is blank (not
        set to specific value), the key is not added to the map.
    \end{description}
  \end{itemize}

  \item \textbf{getStatementBuilder} \\
    \texttt{public NetlistStatementBuilder \textbf{getStatementBuilder}()}
  \begin{itemize}
    \item[-] \textit{\textbf{Description}} \\
      Get an empty netlist statement builder for the instance
    \item[-] \textit{\textbf{Returns}}
    \begin{description}
      \item[] builder
    \end{description}
  \end{itemize}
\end{itemize}

\subsection{Callback~Engine}

\begin{lstlisting}[style=java,caption=Extension of Callback Engine,label={lst:tech-cb-engine}]
package edlab.eda.edp.itk.custom;

import edlab.eda.edp.itk.device.CallbackEngine;
import edlab.eda.edp.itk.device.Device;

public class CustomCallbackEngine implements CallbackEngine {

  @Override
  public boolean canHandleCallback(String triggeredParameter) {
    // TODO
  }

  @Override
  public CallbackEngine handleCallback(Device device,
      String triggeredParameter) {
    // TODO
  }
}
\end{lstlisting}

\subsubsection{Device}

\subsubsection{Device Parameter}