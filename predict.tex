\chapter{PREDICT}

The PREDICT (PRimitivE DevIce CharacTerization) toolbox can be used to create 
and access pre-simulated data of primitive integrated IC 
devices e.g. transistors) in \gls{virtuoso}, \gls{matlab} and \gls{python} (tbd).

\section{Characterization}

The first step for creating a PREDICT database, the \gls{skill} command 
\texttt{EDpredictCharDevice} is needed.

\begin{lstlisting}
EDpredictCharDevice(

  ?sEvalLib  sEvalLib

  ?sDutLib  sDutLib
  ?sDutCell sDutCell
  ?sDutView sDutView

  ?sModelName sModelName
  ?sModelType sModelType
  ?sTag sTag

  ?sSimulator sSimulator
  [?lSimulatorOptions lSimulatorOptions]

  [?lTemperatures lTemperatures]
  [?lOverallHierarchy  lOverallHierarchy]
  [?lOperatingPoints lOperatingPoints]

  ?sOverallAccuracy
  [?lIndividualAccuracies lIndividualAccuracies]

  ?lModels lModels

  ?lParameters lParameters
  ?lParametersUnit lParametersUnit

  ?lParameterGrid lParameterGrid

  [?lVoltages lVoltages]
  [?lCurrents lCurrents]

  [?bVerbose bVerbose]

  [?sDir sDir]

  [?lAliasCells lAliasCells]

  ?bCallInitProc bCallInitProc
  ?lCallbacks lCallbacks
  ?bUseInstCDF bUseInstCDF
  ?bAddFormFields bAddFormFields
)=>sPath/nil
\end{lstlisting}

The parameters of the function are:
\begin{description}[font=\tt]
\item[sEvalLib] Library name of existing library (string) CAUTION: 
  This tool will create new cellviews and possibly overwrite existing 
  cellviews in \texttt{sEvalLib}

\item[sDutLib] Library name of device under test (string)

\item[sDutCell] Cell name of device under test (string)

\item[sDutView] View name of device under test (string)

\item[sModelName] Model name of device under test (string)

\item[sModelType] Model type of device under test (string)

\item[sTag] Tag to be annotated to the database,e.g. PDK version (string)

\item[sSimulator] Name of the simulator as string (only “spectre” is supported by now).

\item[lTemperatures] List of temperatures (in C) for which the device should be characterized.

\item[lOverallHierarchy] Hierarchy of the device under test in the netlist as list.

\item[lOperatingPoints] List of operating points that should be saved.

\item[sOverallAccuracy] String, must be either “double” or “float”.

\item[lIndividualAccuracies] This parameter can be used to define individual 
  accuracies in contrast to sOverallAccuracy. This parameter must be a list of 
  two element lists. The first element of a sublist must correspond to an 
  operating point parameter. The second element of the sublist must correspond 
  to a datatype. This value can be either “byte”, “short”, “int”, “long”, 
  “float” or “double”.

\item[lModels] List of models, which are needed for simulation. 
  This list must consist of two or three element lists. If no sections must be 
  specified for a model file, the first element of the sublist must correspond 
  to an identifier. The second element of the sublist must correspond to the 
  path to model (string). If there are several sections for a model file 
  available, a third element must be added to the list. This third element 
  must be a list as well. All sections which should be considered for 
  characterization must be added to this list. The first element of the 
  sublist is an identifier (e.g. TYP, SLOW, FAST, etc.). 
  The second element of the list must correspond to the section of the 
  (spectre) model file (e.g. tm, ws, wp, etc.).


\item[lParameters] List of parameters, which are used for characterization. 
  All elements of the list must be lists as well. These sublists must have a 
  length of 2. The first element of such a sublist must correspond to the name 
  of the parameter as string or symbol. The second sublist is a list of 
  sampling points.

\item[lParametersUnit] List of units for parameters. This parameter is a list 
  of two list elements The sublist must contain the parameter as string as
   first entry and the corresponding unit.

\item[lParameterGrid] For parameters of a device, a grid can be specified. 
  This parameter must be a list consisting of two element lists. The first 
  element of such a sublist must correspond to a parameter of the device 
  (must be specified in lParameters).


\item[lVoltages] List of voltages, which are used for characterization. 
  All elements of the list must be lists as well. These sublists must have a 
  length of 2. The first element of the first sublist is the identifier for the 
  corresponding nets. A single ended voltage is specified by providing the net
  name as string or symbol, a differential voltage is specified by providing the 
  two nets as a list. The second sublist is a list of sampling points

\item[lCurrents] List of currents, which are used for characterization. 
  All elements of the list must be lists as well. These sublists must have a 
  length of 2. The first element of the first sublist is the identifier for the 
  corresponding nets. A current between a net and ground is specified by 
  providing the (non-ground) net as string. A current between to nets is 
  specified by providing the two nets as a list. The second sublist is a list 
  of sampling points.

\item[bVerbose] Verbose output

\item[sDir] Path where the PREDICT database is created (string). When this 
  parameter is not provided, “./” is used.

\item[lAliasCells] References devices that should be referenced in addition 
  to this database. This parameter is a list, consisting of two element lists.
  The fist element of such an sublist must correspond to the library name. 
  The second sublist element must correspond to the cell name.

\item[bCallInitProc] Needed for CDF parameter evaluation (bool). 
  Check CCSinvokeCdfCallbacks.il script of Cadence.

\item[lCallbacks] Needed for CDF parameter evaluation (bool). 
  Check CCSinvokeCdfCallbacks.il script of Cadence.

\item[bUseInstCDF] List of parameters. Needed for CDF parameter evaluation (bool). 
  Check CCSinvokeCdfCallbacks.il script of Cadence.

\item[bAddFormFields] Needed for CDF parameter evaluation (bool). 
  Check CCSinvokeCdfCallbacks.il script of Cadence.
\end{description}

\todo[inline]{Describe PREDICT/Update from HTML}

\section{Explorer}

\begin{figure}[H]
  \centering
   \begin{tikzpicture}
     \node[] () at (0,0) 
         {\includegraphics[scale=0.4]{./figures/predict_explorer_1.png}};
   \end{tikzpicture}
  \caption{PREDICT Explorer}
  \label{fig:predict-explorer-1}
\end{figure}

\begin{figure}[H]
  \centering
   \begin{tikzpicture}
     \node[] () at (0,0) 
         {\includegraphics[scale=0.4]{./figures/predict_explorer_2.png}};
   \end{tikzpicture}
  \caption{PREDICT Explorer}
  \label{fig:predict-explorer-2}
\end{figure}

\begin{figure}[H]
  \centering
   \begin{tikzpicture}
     \node[] () at (0,0) 
         {\includegraphics[scale=0.4]{./figures/predict_explorer_3.png}};
   \end{tikzpicture}
  \caption{PREDICT Explorer}
  \label{fig:predict-explorer-3}
\end{figure}


\begin{figure}[H]
  \centering
   \begin{tikzpicture}
     \node[] () at (0,0) 
         {\includegraphics[scale=0.4]{./figures/predict_explorer_4.png}};
   \end{tikzpicture}
  \caption{PREDICT Explorer}
  \label{fig:predict-explorer-4}
\end{figure}
