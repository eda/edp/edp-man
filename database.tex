\chapter{Database}

The chart shown in Fig.~\ref{fig:working-environment} details the
workflow from the \textit{expert} view.
On the right side we can see the existing design environment, 
i.e. \gls{virtuoso}.
The design environment operates in a dedicated working directory, which we
refer to as \texttt{cdsWorkDir}%
\footnote{This directory contains files such as \texttt{cds.lib} or
\texttt{.cdsinit} .}.
In the design environment, we must start a so called socket for
communication with the \gls{edp}.
The socket is in principle an machine-internal port that is able
to extract data from the design environment and forwards it 
to the \gls{edp} core and reversed.
Establishing the socket does not cost any additional licenses, so
there is no disadvantage when the socket runs every time
when you start \gls{virtuoso}.

\begin{figure}[H]
  \centering
   \begin{tikzpicture}

      \node[scale=2, inner sep=0.5pt] (user) at (0,6) {\faUser};
      \node[anchor=south] at (user.north) {expert};

      \path[-{Latex[round,scale=0.95]}, densely dashed, semithick] 
        (user.south west) edge [out=-150,in=90] (-1.95,2);

      \path[-{Latex[round,scale=0.95]}, densely dashed, semithick] 
        (user.west) edge [out=-150,in=75] (-4.05,2);

      \path[-{Latex[round,scale=0.95]}, densely dashed, semithick] 
        (user.south east) edge [out=-30,in=90] (3,0.5);

      \node[font=\small\it] at (3,5.5)  {manual workflow};
      \node[font=\small\it] at (-3,5.5) {\gls{edp} workflow};      

      \draw[] (-2.9,1.0) rectangle (-1,2);
      \node[] at (-1.95,1.5) {\gls{matlab}};
      \draw[] (-5,1.0) rectangle (-3.1,2);
      \node[] at (-4.05,1.5) {\gls{python}};

      \draw[] (-5,-0.5) rectangle (-1,0.5);
      \node[] at (-3,0) {Core};

      \draw[-{Latex[round,scale=0.7]},shorten <=1pt,shorten >=1pt] 
        (-1.75,0.5) -- (-1.75,1);
      \draw[{Latex[round,scale=0.7]}-,shorten <=1pt,shorten >=1pt]
        (-2.05,0.5) -- (-2.05,1);

      \draw[-{Latex[round,scale=0.7]},shorten <=1pt,shorten >=1pt]
        (-3.95,0.5) -- (-3.95,1);
      \draw[{Latex[round,scale=0.7]}-,shorten <=1pt,shorten >=1pt]
        (-4.15,0.5) -- (-4.15,1);

      \draw[] (1,-0.5) rectangle (5,0.5);
      \node[] at (3,0) {Design Environment};

      \node[scale=2,inner sep=0.5pt] (folderleft) at (-6.5,0) {\faFolderOpenO};
      \draw (folderleft.east) -- (-5.1,0);
      \node[anchor=south,font=\tt] at (folderleft.north) {edpWorkDir};

      \node[scale=2,inner sep=0.5pt] (folderright) at (6.5,0) {\faFolderOpenO};
      \draw (folderright.west) -- (5.1,0);
      \node[anchor=south,font=\tt] at (folderright.north) {cdsWorkDir};

      \node[font=\small,anchor=south] at (0,-1.45) {Socket};
      \fill[black] (-0.15,-1.75) rectangle (0.15,-1.45);

      \draw (3,-0.5)  -- (3,-1.6)  -- (0.15,-1.6);
      \draw (-3,-0.5) -- (-3,-1.6) -- (-0.15,-1.6);
    \end{tikzpicture}
  \caption{Working~Environment}
  \label{fig:working-environment}
\end{figure}

The socket is started for the menu bar of the \gls{ciw} with
\begin{lstlisting}
EDP |\faAngleRight| Start Socket
\end{lstlisting}
As a result, a info message, e.g.
\begin{lstlisting}
**INFO** Socket #41437 started in "/home/schweikardt/eda-workspace/dsgn/nopdk"
\end{lstlisting}
is printed in the \gls{ciw}.
The socket is stopped in the menu bar of the \gls{ciw} with
\begin{lstlisting}
EDP |\faAngleRight| Stop Socket (41437)
\end{lstlisting}
As an alternative, the socket can be started with the \gls{skill} command
\begin{lstlisting}
(EDedpStartSocket)
\end{lstlisting}
and returns the port number when started correctly and \texttt{nil} otherwise%
\footnote{You may add this command to the \texttt{.cdsinit} when the
socket should be started always with \gls{virtuoso}.}.
The function 
\begin{lstlisting}
(EDedpStopSocket)
\end{lstlisting}
is used for stopping the circuit.

The left hand side of the chart (Fig.~\ref{fig:working-environment})
shows how the expert is able to  write and execute an \gls{edp}.

You should not work with the \gls{edp} in the \gls{virtuoso}
working directory \texttt{cdsWorkDir}, it is recommend to use a different 
directory \texttt{edpWorkDir}.
The \gls{matlab} or \gls{python} commands are forwarded to the \textit{Core}.
When needed, the core connects to the socket for exchanging data with the
design environment.

\bigskip

The main entry point for every \gls{edp} is the class \texttt{Database}.
The database is used to
\begin{itemize}
  \item import existing schematic from the design environment (\gls{virtuoso})
   to \gls{edp} toolbox and reversed
  \item access schematics
  \item change parameters of devices in circuits
  \item create tests for circuit characterization
  \item tweaking functions for design parameters
  \item ...
\end{itemize}
In addition, all other \gls{edp} components, e.g.
the Schematic~Generator (conf.~section~\ref{sec:sch-gen}) are
started in the \texttt{Database} (explained later).

First you must import the database in the \gls{python} code with
\begin{lstlisting}
from edp import .
\end{lstlisting}

To initialize a \texttt{Database}, you must provide a \gls{json} file 
that references all directories, the \glspl{pdk}, the \glspl{lut} and so on.
The listing below shown a template of such an initialization file.
\begin{lstlisting}[]
{ "session"       : true
, "cdsWorkDir"    : "/home/johndoe/eda-workspace"
, "logDir"        : "/home/johndoe/log"
, "appendLogfile" : true
, "techFiles"     : ["./tech1.xml", "/home/techs/tech2.xml"]
, "predictDBs"    : ["./nmos", "/home/predict/pmos"]
, "workLibs"      : ["edp_dsgn", "edp_tb"]
, "evalLib"       : "evalLib"
, "simDir"        : "/home/sim"
, "simTimeout"    : 10
}
\end{lstlisting}

The description below details the contents of the above shown \gls{json}
\begin{description}[font=\tt]
    \item[session] When a connection to \gls{virtuoso} is established, 
      this property must be set true.
    \item[cdsWorkDir] This path must point to the working directory where
      \gls{virtuoso} is started.
      When no value is specified, the current working directory is used 
      (not recommended).
    \item[logDir] With this parameter you can control where the \gls{edp}
      logfile is stored.
      The name of the file that is created in this directory is always
      \texttt{edp.log}.
      When you do not specify this value, the logfile is created in the
      current working directory \texttt{edpWorkDir}.
    \item[appendLogfile] The optional boolean property controls that
      when a new \gls{edp} Database is started, that the new created logfile
      is appended to the existing one (\textit{default} = \texttt{false}).
    \item[infoMessages] The optional boolean property controls whether
      \textit{INFO} messages are printed to the console 
      (\textit{default} = \texttt{true}).
    \item[debugMessages] The optional boolean property controls whether
      \textit{DEBUG} messages are printed to the console 
      (\textit{default} = \texttt{false}).   
    \item[techFiles] The array references all technology files that are 
      used in the database. 
    \item[workLibs] The elements of this array reference the \textit{design}
      libraries in \gls{virtuoso} that are utilized during \gls{edp}
      creation and execution%
      \footnote{It is recommended to consider only a dedicated number of 
        libraries from \gls{virtuoso} in the \gls{edp}}.
      Do not include the \gls{pdk} libraries here, they have been already 
      considered in the \texttt{techFiles}.
    \item[predictDBs] The array references all PREDICT databases files that 
      are used in the database. 
    \item[evalLib] You must specify a library in \gls{virtuoso} for 
      temporary cellviews. 
      The tool will create and delete cellviews in this library 
      during evaluation of \gls{edp} code.
      Do not utilize any libraries that contains real designs for this 
      purpose%
      \footnote{We recommend to define a new library in every project where
        \gls{edp} is used. Name this library \texttt{evalLib}.}.
    \item[simDir] This path must point to the directory where simulation
       results are stored.
       When you do not provide any path, \texttt{/tmp} is used as
       default.
    \item[simTimeout] This value (in s) corresponds to the timeout that is 
      used for simulation.
      The \gls{edp} Core executes simulations interactively,
      i.e. the corresponding license is checkout when the simulation is
      initialized. 
      When no simulation is run for the here referenced timeout,
      the simulation will shutdown and release the license.
\end{description}

We recommend to use \texttt{.edpinit.json} as name for this initialization file.
When the \gls{json} is available you can initialize the \texttt{Database}
with
\begin{lstlisting}
db = Database.init()
\end{lstlisting}
When you have utilized a different file name for the \gls{json},
please reference the path to the file as parameter.
\begin{lstlisting}
db = Database.init('myedpinit.json')
\end{lstlisting}

Please check the logfile for errors or warnings at startup.

The database can be terminated by calling the method \texttt{quit}:
\begin{lstlisting}
db.quit()
\end{lstlisting}
When a database is terminated, all associated database objects 
(schematics, netlists,...) are corrupted and
cannot be used.
All databases can be terminated with
\begin{lstlisting}
Database.quitAll()
\end{lstlisting}

\bigskip

A \texttt{Database} contains the data that is created and modified during 
design.
An abstract representation of the \texttt{Database} is given in 
Fig.~\ref{fig:technology-db}.
There, multiple libraries are referenced.
Every library contains one or multiple cells%
\footnote{You may also call this a \textit{Module}.}.
A cell is characterized by its interface, i.e. which internal signals are
accessible from the outside word (pins).
A cell can have different representations by so-called views.
A view can be a schematic (structural representation) or a
behavioral representation, e.g. in \gls{veriloga}.
In most cases, a cell contains only a single schematic view.

\begin{figure}[H]
  \centering
  \begin{tikzpicture}[font=\tt\small]
   
  \node {Database}
      child [xshift=-5.5cm] {
        node {Library \#1}
        child [xshift=-0.2cm] {
          node {$\hdots$}
        }
        child [xshift=0.2cm] {
          node {$\hdots$}
        }
      }
      child [xshift=0.0cm]{
        node [] {Library \#2}
        child [xshift=-2.75cm] {
          node {Cell \#1}
          child [xshift=-0.5cm] {
            node {$\hdots$}
          }
        }
        child [xshift=0cm] {
          node {Cell \#2}
          child [xshift=-0.5cm] {
            node {View \#1}
          }
          child [xshift=0cm] {
            node {View \#2}
          }
          child [xshift=0.5cm] {
            node {$\hdots$}
          }
        }
        child [xshift=2.75cm] {
          node {$\hdots$}
        }
      }
      child [xshift=5.5cm]{
        node [] {Library \#3}
        child [xshift=-0.2cm] {
          node {$\hdots$}
        }
        child [xshift=0.2cm] {
          node {$\hdots$}
        }
      };
   
  \end{tikzpicture}
  \caption{Main structure of the \texttt{Database}}
  \label{fig:technology-db}
\end{figure}


From the initilaized database, you can get all library name.
\begin{lstlisting}
db.getLibraryNames()
>> ['edp_dsgn', 'edp_tb']
\end{lstlisting}
The return value should match with \texttt{workLibs} from the 
initialization file.

\medskip

The names of all cells can be accessed with
\begin{lstlisting}
db.getCellNames('edp_dsgn')
>> ['rc', 'opamp', 'resdivider']
\end{lstlisting}
As a result, you can access all cell names in the library as a list.
You can access a particular cell in the database with
\begin{lstlisting}
cell = db.getCell('edp_dsgn', 'opamp')
\end{lstlisting}
The return value is of type \texttt{DatabaseCell}.
From the cell you can get the library name
\begin{lstlisting}
cell.getLibraryName()
>>'edp_dsgn'
\end{lstlisting}
and cell name
\begin{lstlisting}
cell.getCellName()
>>'opamp'
\end{lstlisting}
and also the pins
\begin{lstlisting}
cell.getPins()
>> ['INP', 'INN', 'OUT', 'VDD', 'VSS']
\end{lstlisting}

Similar than for the cell, you can access all sub-view names 
of a cell with
\begin{lstlisting}
cell.getViewNames()
>> ['schematic1', 'schematic2']
\end{lstlisting}

You can traverse down in hierarchy to a view from the cell
\begin{lstlisting}
view = cell.getView('schematic1')
\end{lstlisting}
or directly in the \texttt{Database}
\begin{lstlisting}
view = db.getView('edp_dsgn', 'opamp','schematic1')
\end{lstlisting}


\todo[inline]{Explain other data structures}
