\chapter{Introduction}

\tikzset{
  interm/.pic = {
    \draw[fill=black]   (-0.15,0    ) 
                     -- (-0.15,0.15 ) 
                     -- (0    ,0.15 ) 
                     -- (0.15 ,0    ) 
                     -- (0    ,-0.15) 
                     -- (-0.15,-0.15) 
                     -- cycle;
    \node[anchor=east,font=\tt] at (-0.15,0) {\tikzpictext};
  }
}

\tikzset{
  outerm/.pic = {
    \draw[fill=black]   (-0.15,0    ) 
                     -- (-0.15,0.15 ) 
                     -- (0    ,0.15 ) 
                     -- (0.15 ,0    ) 
                     -- (0    ,-0.15) 
                     -- (-0.15,-0.15) 
                     -- cycle;
    \node[anchor=west,font=\tt] at (0.15,0) {\tikzpictext};
  }
}
\tikzset{
  supterm/.pic = {
    \draw[fill=black]   (-0.15+0.075,0.15 )
                     -- (0.075   ,0.15 ) 
                     -- (0.15+0.075 ,0    ) 
                     -- (0.075    ,-0.15) 
                     -- (-0.15+0.075,-0.15) 
                     -- (-0.3+0.075  ,0   ) 
                     -- cycle;
    \node[anchor=east,font=\tt] at (-0.3+0.075,0) {\tikzpictext};
  }
}

This document is the \textit{User~Guide} for the \gls{edp} toolbox
for automating the design of analog integrated circuits.
An \gls{edp} is an executable script, which captures the strategy of a 
an analog designer when designing an analog circuit and makes it reusable.

\section{Background}

Simply speaking, the design of analog integrated circuits can be split up
in the two different phases, \textit{circuit~design} and 
\textit{physical~design} (Fig.~\ref{fig:analog-flow}). 

\medskip

Circuit~design describes the task to "transform" a given specification 
in a sized schematic.
The first information of every specification is which type
of circuit is implemented, e.g. an common~source~amplifier.
Every circuit type has several \textit{performance~parameters},
that are associated with it, e.g. gain or bandwidth.

\medskip

The resulting schematic contains several instances of primitive devices
(transistors, resistors).
The instances are connected with wires.
The devices and the connections between them form the \textit{topology}
of the circuit.
Each instance has several (geometric) parameters that must be 
found, e.g. width or length.
Finding suitable parameter values for these parameters is called 
\textit{sizing}.

\medskip

During \textit{physical~design}, a layout is created that 
implements the sized schematic (not part of this document).

\begin{figure}[h]
  \centering
  \begin{circuitikz} 
  \ctikzset{tripoles/nigfete/height=0.9}
  \ctikzset{tripoles/nigfete/width=0.5}
  \ctikzset{bipoles/resistor/width=0.5}
  \ctikzset{bipoles/resistor/height=0.2}

  \begin{scope}[shift={(-6.25,0)}]

    \fill[gray!10!white] (-2,-1) rectangle (2,4.5);
    \fill[gray!40!white] (-2,4) rectangle (2,4.5);
    \node[] at (0,4.25) {Specification};

    %\node[scale=6] at (0,0) {\faFileO};

    \draw[rounded corners=2pt, semithick] 
      (-1.5,-0.5) -- (1.5,-0.5) -- (1.5,2.75) -- 
      (0.75,3.5) --(-1.5,3.5) -- cycle;
    \draw[rounded corners=2pt, semithick] 
      (1.5,2.75) -- (0.75,3.5) -- (0.75,2.75) -- cycle;

    \node[font=\footnotesize\bfseries, text width=2.75cm,left, anchor=west] 
      at (-1.5,2.25) {Common~Source Amplifier};

    \node[font=\footnotesize, text width=3cm,left, anchor=west] 
      at (-1.5,1.65) {- DC Gain=10};

    \node[font=\footnotesize, text width=3cm,left, anchor=west] 
      at (-1.5,1.05) {- BW $>$ \SI{1}{\kilo\hertz}};

    \node[font=\footnotesize, text width=3cm,left, anchor=west] 
      at (-0.2,0.45) {$\vdots$};
  \end{scope}

  \node[ font=\it
       , text width = 2.0cm
       , anchor=south
       , align = center
       ] at (-3.15,2.2) {circuit design};

  \node[ single arrow
       , draw
       , single arrow head extend=0.2em
       , inner ysep=0.2em
       , transform shape
       , line width=0.05em
       , anchor = center
       , fill = gray
       , scale = 3
       ] at (-3.25,1.5) {};

  \begin{scope}[shift={(0,0)}]

    \fill[gray!10!white] (-2,-1) rectangle (2,4.5);
    \fill[gray!40!white] (-2,4) rectangle (2,4.5);

    \node[] at (0,4.25) {Schematic};
  
    \node[nigfete, anchor=source] (mos) at (0,0) {};

    \draw (mos.drain) to [short,-*] ++ (0,0.1) coordinate(x)
                      to [short,-]  ++ (0,0.1)
                      to [R] ++ (0,1.25)
                      to [short,-]  ++ (0,0.5) coordinate(vdd);
    \pic[pic text=VDD] at (vdd) {supterm};

    \draw (x) to [short,-] ++ (1,0) coordinate(o);
    \pic[pic text=O] at (o) {outerm};

    \draw (mos.gate) to [short,-] ++ (-0.5,0) coordinate(in);
    \pic[pic text=I] at (in) {interm};

    \draw (mos.source) to [short,-] ++ (0,-0.5) coordinate(vss);
    \pic[pic text=VSS] at (vss) {supterm};

    \node[ anchor    = west
         , inner sep = 1pt
         , font      = \scriptsize\tt
         , rotate    = 0
         , right     = 5pt
         ] () at ($(vdd)!0.45!(x)$) {R0};  

    \node[ anchor    = west
         , inner sep = 1pt
         , font      = \tiny\tt
         , rotate    = 0
         , right     = 5pt
         ] () at ($(vdd)!0.65!(x)$) {w=1u};  

    \node[ anchor    = west
         , inner sep = 1pt
         , font      = \tiny\tt
         , rotate    = 0
         , right     = 5pt
         ] () at ($(vdd)!0.75!(x)$) {w=20u};      

    \node[ anchor    = west
         , inner sep = 1pt
         , font      = \tiny\tt
         , rotate    = 0
         , right     = 5pt
         ] () at ($(vdd)!0.85!(x)$) {b=4};  

    \node[ anchor    = west
         , inner sep = 1pt
         , font      = \scriptsize\tt
         , rotate    = 0
         ] () at ($(mos.bulk)!0.4!(mos.drain)$) {M1};

    \node[ anchor    = west
         , inner sep = 1pt
         , font      = \tiny\tt
         , rotate    = 0
         ] () at ($(mos.bulk)!0.17!(mos.source)$) {m=1};  

    \node[ anchor    = west
         , inner sep = 1pt
         , font      = \tiny\tt
         , rotate    = 0
         ] () at ($(mos.bulk)!0.45!(mos.source)$) {w=10u};

    \node[ anchor    = west
         , inner sep = 1pt
         , font      = \tiny\tt
         , rotate    = 0
         ] () at ($(mos.bulk)!0.72!(mos.source)$) {l=1u};

  \end{scope}

  \node[ font=\it
       , text width = 2.0cm
       , anchor=south
       , align = center
       ] at (3.15,2.2) {physical design};

  \node[ single arrow
       , draw
       , single arrow head extend=0.2em
       , inner ysep=0.2em
       , transform shape
       , line width=0.05em
       , anchor = center
       , fill = gray
       , scale = 3
       ] at (3.25,1.5) {};

  \begin{scope}[shift={(6.25,0)}]

    \fill[gray!10!white] (-2,-1) rectangle (2,4.5);
    \fill[gray!40!white] (-2,4) rectangle (2,4.5);
    \node[] at (0,4.25) {Layout};

    \begin{scope}[scale=0.3,shift={(-5,-0.5)}]
      \input{./tikz/cs-lay}
    \end{scope}
  \end{scope}

  \end{circuitikz}
  \caption{Analog~integrated~circuit design~flow}
  \label{fig:analog-flow}
\end{figure}

\section{Automation~Paradigms}

The majority of all analog designs are still created in a manual fashion.
Fur sure, there are \gls{eda} tools available that provide a schematic and layout 
entry, including verification tools (simulation, DRC, LVS), but the
creative labour must be still done by the designers.
Using suitable \textit{automation} methods, this task can be speed up,
resulting in a shorter development time.

\subsection{Optimization}
A very prominent approach to automate the sizing of analog integrated 
circuits is to use \textit{optimizers}.
As a first step, one must formalize the specification as \textit{objectives}
and \textit{constraints}.
This formalization step is sometimes done in a semi-automatic fashion.

\medskip

Then, this information is forwarded to the optimization loop,
which consists of an \textit{optimization~algorithm} and
an \textit{evaluation~engine}.
The optimization~algorithm proposes a new \textit{candidate},
which is a set of geometric parameters and forwards it
to the evaluation~engine.
The evaluation~engine analyzes the circuit and extracts the 
performance (parameters) from the circuit.
The evaluation~engine can be implemented in various different ways, e.g. with
\begin{itemize}
  \item an analog~simulator
  \item symbolic~equations
  \item surrogate~models
\end{itemize}  
The optimization~algorithm compares the performance with the objectives
and constrains and terminates when the result satisfies the specification.
When not, a new candidate is proposed and the loop iterates until a 
suitable specification is found.

\begin{figure}[h]
  \centering
  \begin{tikzpicture}[every node/.style={inner sep=1pt,outer sep=1pt}]
  \node [ rectangle
        , rounded corners
        , text centered
        , draw=black
        , very thick
        , minimum width=5cm
        , minimum height=1cm
        ] (expl) at (0,1.5) {Optimization~Algorithm};

  \node [ rectangle
        , rounded corners
        , text centered
        , draw=black
        , very thick
        , minimum width=5cm
        , minimum height=1cm
        ] (eval) at (0,-1.5){Evaluation~Engine};

  \draw[->, >=latex, black, line width=0.1cm,out=315,in=45]
    (expl.east) to node[black,left]{Sizing}  (eval.east);

  \draw[->, >=latex, black, line width=2pt,bend left=45]
    (0,0.7)  to node[black]{}  (0.7,0);
  \draw[->, >=latex, black, line width=2pt,bend left=45]
    (0.7,0)  to node[black]{}  (0,-0.7);
  \draw[->, >=latex, black, line width=2pt,bend left=45]
    (0,-0.7) to node[black]{}  (-0.7,0);
  \draw[->, >=latex, black, line width=2pt,bend left=45]
    (-0.7,0) to node[black]{}  (0,0.7);

  \draw [->, >=latex, black, line width=0.1cm,out=135,in=235]
    (eval.west) to node[black,right]{Performance} (expl.west);

  \node [ draw
        , thick
        , shape=rectangle
        , minimum width=6.8cm
        , minimum height=4.5cm
        , anchor=center
        , rounded corners=5mm
        , dashed
        ] (optimizer) at (0,0) {};

  \node [ draw
        , thick
        , shape=rectangle
        , minimum width=2cm
        , minimum height=1cm
        , anchor=center
        , text width=2.2cm
        , align=center
        ] (parameter) at (0,3.3) {Objectives, Constraints};

  \node [ draw
        , thick
        , shape=rectangle
        , minimum width=2cm
        , minimum height=1cm
        , anchor=center
        , text width=2.2cm
        , align=center
        , rounded corners
        ] (sol) at (0,-3) {Sized Schematic};

  \node [ align=center
        , draw=black
        , rounded corners
        , minimum height=1cm
        , thick
        , text width=2.2cm
        ] (func) at (0,4.9) {Specification};

  \node [ draw=black
        , fill=gray
        , single arrow
        , rotate=270
        , inner sep=2mm
        , single arrow head extend=1.5mm
        , anchor=center
        , yscale=0.5
        ] at (0,4.1) {};

  \node [ draw=black
        , fill=gray
        , single arrow
        , rotate=270
        , inner sep=2mm
        , single arrow head extend=1.5mm
        , anchor=center
        , yscale=0.5
        ] at (0,2.5) {};

  \node [ draw=black
        , fill=gray
        , single arrow
        , rotate=270
        , inner sep=2mm
        , single arrow head extend=1.5mm
        , anchor=center
        , yscale=0.5
        ] at (optimizer.south){};
  \end{tikzpicture}
  \caption{Sizing with optimizers}
  \label{fig:sizing-optimizers}
\end{figure}

There are various existing tools available the support sizing of circuits
with optimizers, e.g. WiCkeD~\cite{wicked} or the 
\gls{cds} Local and Global optimizer~\cite{ade-assembler-userguide}.

The advantage of such a method is, that it is generic and can applied to 
arbitrary circuits.
In reality, a lot of time must be spend in \textit{formalization}, 
a work that is unfavoured by analog designers.
Furthermore, the runtime of such a method increases drastically when
a lot of parameters and/or objectives must be considered.
Maybe the biggest disadvantage of \textit{optimizers} is, that
there is no insight in how the optimizer achieved a particular result.

\subsection{Procedures}

Since optimizer are used in reality only on a small subset of designs,
we have to take a step back an look at the state of the art.
Most analog circuits are, even nowadays, handcrafted with little
use of automation.
Every experienced analog designer has a certain strategy in mind when
designing a particular circuit.
The flow chart shown in Fig.~\ref{fig:manual-circuit-design-fow}
illustrates a typical analog design flow abstractly.
It consists of several \textit{tasks}, \textit{decisions} and 
\textit{iterations}.

\medskip

When you look an analog designer over the shoulder, then you can 
easily identify these tasks, such as
\begin{itemize}
  \item draw a circuit,
  \item create a simulation setup,
  \item initialize size a circuit with equations
\end{itemize}
and decisions and iterations
\begin{itemize}
  \item test different topologies,
  \item size a circuit with a simulator in the loop.
\end{itemize}

\begin{figure}[h]
  \centering
  \begin{tikzpicture}[every node/.style={inner sep=1pt,outer sep=1pt},scale=1.25]

    \node[ fill=black
         , circle
         , minimum size=1pt
         ] (origin) at (0,2) {};

    \node[anchor=south] at (origin.north) {Start};
    
    \node[ draw
         , black
         , minimum width=1cm
         , minimum height=0.25cm
         , anchor=center
         , align=center
         , rounded corners
         ] (A) at (0,1.5) {};

    \node[font=\small\it] (task) at (3,1.75) {task};
    \draw[-Latex,gray] (task.west) -- (A.east);

    \node [ draw
          , black
          , minimum width=1cm
          , minimum height=0.25cm
          , anchor=center
          , align=center
          , shape=diamond
          ] (B) at (0,0.75) {};

    \node[font=\small\it] (dec) at (3,1.25) {decision};
    \draw[-Latex,gray] (dec.west) -- (B.north east);  

    \node[font=\small\it] (iter) at (3.75,0) {iteration};
    \draw[-Latex,gray] (iter.west) -- (2.5,0);  

    \draw [-{Latex[round]}] (A.south) --  (B.north);

    \node [ draw
          , black
          , minimum width=1cm
          , minimum height=0.25cm
          , anchor=center
          , align=center
          , rounded corners
          ] (C) at (0,0) {};

    \draw [-{Latex[round]}] (B.south) --  (C.north);

    \node[ draw
         , black
         , minimum width=1cm
         , minimum height=0.25cm
         , anchor=center
         , align=center
         , shape=diamond
         ] (D) at (0.75,-0.9) {};

    \node[ draw
         , black
         , minimum width=1cm
         , minimum height=0.25cm
         , anchor=center
         , align=center
         , rounded corners
         ] (E) at (1.5,0) {};

    \draw [-{Latex[round]}] (C.south) --  (0,-0.3) -- (0.75,-0.3)-- (D.north);

    \draw [-{Latex[round]}] (E.south) --  (1.5,-0.3) -- (0.75,-0.3)-- (D.north);

    \draw [-{Latex[round]}] (B.east) --  (1.5,0.75) -- (E.north);

    \draw [-{Latex[round]}] (D.east) --  (2.5,-0.9) -- (2.5,0);
    \draw  (D.east) --  (2.5,-0.9) -- (2.5,0.75) -- (1.5,0.75);

    \node[ draw
         , black
         , minimum width=1cm
         , minimum height=0.25cm
         , anchor=center
         , align=center
         , rounded corners
         ] (F) at (-2,1.5) {};
    
    \draw [-{Latex[round]}] (origin.south)  -- (A.north);

    \draw [-{Latex[round]}] (D.south) --  (0.75,-1.25) -- 
      (-0.75,-1.25)-- (-0.75,2) -- (-2,2) -- (F.north);

    \node [ draw
          , black
          , minimum width=1cm
          , minimum height=0.25cm
          , anchor=center
          , align=center
          , shape=diamond
          ] (G) at (-2,0.75) {};
    
    \draw [-{Latex[round]}] (F.south)  -- (G.north);
    
    \node[ draw
         , black
         , minimum width=1cm
         , minimum height=0.25cm
         , anchor=center
         , align=center
         , rounded corners
         ] (H) at (-2,0) {};
    
    \draw [-{Latex[round]}] (G.south)  -- (H.north);
    
    \node[ draw
         , black
         , minimum width=1cm
         , minimum height=0.25cm
         , anchor=center
         , align=center
         , rounded corners
         ] (I) at (-2,-1) {};
    
    \draw [-{Latex[round]}] (H.south)  -- (I.north);
    
    \draw [-{Latex[round]}] (G.east)  -- (-1.25,0.75)  
      -- (-1.25,-0.45) -- (-2,-0.45);  

    \coordinate (stop) at (0,-2) {};
    
    \draw [-{Latex[round]}] (I.south)  -- (-2,-1.5)  
      -- (0,-1.5)  -- (stop.north) ;

    \node[ fill=white
         , circle
         , draw = black
         , minimum size=1pt
         , anchor = north
         ] at (stop) {};

    \node[anchor=north, inner sep=3pt] (outc) at (stop.south) {Stop};
  \end{tikzpicture}
  \caption{Manual circuit~design flow}
  \label{fig:manual-circuit-design-fow}
\end{figure}
This is the point where \textit{procedural~automation} comes into play.
The concept here is that the experience based strategy of an 
analog designer is captured in an executable script, i.e.
every task or decision is represented by one or multiple commands.
Then, parameters are attached to the script. 
The script can be executed for different parameters (Fig.~\ref{fig:procedure}),
i.e. specifications.
You can compare procedural automation with a cooking recipe that is able to
cook itself.
This method is already widely used for the automation of 
\textit{physical~design} by so called \glspl{pcell}.
\begin{figure}[h]
  \centering
  \begin{tikzpicture}[every node/.style={inner sep=1pt,outer sep=1pt}]
    \node [ rectangle
          , rounded corners=5mm
          , text centered
          , draw=black
          , dashed
          , semithick
          , minimum width=6.5cm
          , minimum height=5.25cm
          ] (optimizer) at (0,0.375) {};

    \node [ draw
          , thick
          , rectangle
          , minimum width=2cm
          , minimum height=1cm
          , anchor=center
          , text width=2.2cm
          , align=center
          , fill=white
          ] (parameter) at (0,3) {Parameters};
            
    \node [ draw
          , thick
          , rectangle
          , minimum width=2cm
          , minimum height=1cm
          , anchor=center
          , text width=2.2cm
          , align=center
          , rounded corners
          ] (sol) at (0,-3.3) {Sized Schematic};

    \node [ align=center
          , draw=black
          , rounded corners
          , minimum height=1cm
          , thick,text width=2.2cm
          ] (func) at (0,4.6) {Specification};

    \node [ draw=black
          , fill=gray
          , single arrow
          , rotate=270
          , inner sep=2mm
          , single arrow head extend=1.5mm
          , yscale=0.5
          ] at (0,3.8)  {};

    \node [ draw=black
          , fill=gray
          , single arrow
          , rotate=270
          , inner sep=2mm
          , single arrow head extend=1.5mm
          , yscale=0.5
          ] at (0,2.55) {};

    \node [ draw=black
          , fill=gray
          , single arrow
          , rotate=270
          , inner sep=2mm
          , single arrow head extend=1.5mm
          , anchor=center
          , yscale=0.5
          ] at (optimizer.south)  {};

    \node[ fill=black
         , circle
         , minimum size=1pt
         ] (origin) at (0,2) {};
    
    \node[ draw
         , black
         , minimum width=1cm
         , minimum height=0.25cm
         , anchor=center
         , align=center
         , rounded corners
         ] (A) at (0,1.5) {};

    \node [ draw
          , black
          , minimum width=1cm
          , minimum height=0.25cm
          , anchor=center
          , align=center
          , shape=diamond
          ] (B) at (0,0.75) {};

    \draw [-{Latex[round]}] (A.south) --  (B.north);

    \node [ draw
          , black
          , minimum width=1cm
          , minimum height=0.25cm
          , anchor=center
          , align=center
          , rounded corners
          ] (C) at (0,0) {};

    \draw [-{Latex[round]}] (B.south) --  (C.north);

    \node[ draw
         , black
         , minimum width=1cm
         , minimum height=0.25cm
         , anchor=center
         , align=center
         , shape=diamond
         ] (D) at (0.75,-0.9) {};

    \node[ draw
         , black
         , minimum width=1cm
         , minimum height=0.25cm
         , anchor=center
         , align=center
         , rounded corners
         ] (E) at (1.5,0) {};

    \draw [-{Latex[round]}] (C.south) --  (0,-0.3) -- (0.75,-0.3)-- (D.north);

    \draw [-{Latex[round]}] (E.south) --  (1.5,-0.3) -- (0.75,-0.3)-- (D.north);

    \draw [-{Latex[round]}] (B.east) --  (1.5,0.75) -- (E.north);

    \draw [-{Latex[round]}] (D.east) --  (2.5,-0.9) -- (2.5,0);
    \draw  (D.east) --  (2.5,-0.9) -- (2.5,0.75) -- (1.5,0.75);

    \node[ draw
         , black
         , minimum width=1cm
         , minimum height=0.25cm
         , anchor=center
         , align=center
         , rounded corners
         ] (F) at (-2,1.5) {};
    
    \draw [-{Latex[round]}] (origin.south)  -- (A.north);

    \draw [-{Latex[round]}] (D.south) --  (0.75,-1.25) -- 
      (-0.75,-1.25)-- (-0.75,2) -- (-2,2) -- (F.north);

    \node [ draw
          , black
          , minimum width=1cm
          , minimum height=0.25cm
          , anchor=center
          , align=center
          , shape=diamond
          ] (G) at (-2,0.75) {};
    
    \draw [-{Latex[round]}] (F.south)  -- (G.north);
    
    \node[ draw
         , black
         , minimum width=1cm
         , minimum height=0.25cm
         , anchor=center
         , align=center
         , rounded corners
         ] (H) at (-2,0) {};
    
    \draw [-{Latex[round]}] (G.south)  -- (H.north);
    
    \node[ draw
         , black
         , minimum width=1cm
         , minimum height=0.25cm
         , anchor=center
         , align=center
         , rounded corners
         ] (I) at (-2,-1) {};
    
    \draw [-{Latex[round]}] (H.south)  -- (I.north);
    
    \draw [-{Latex[round]}] (G.east)  -- (-1.25,0.75)  
      -- (-1.25,-0.45) -- (-2,-0.45);  

    \coordinate (stop) at (0,-2) {};
    
    \draw [-{Latex[round]}] (I.south)  -- (-2,-1.5)  
      -- (0,-1.5)  -- (stop.north) ;
  \end{tikzpicture}
  \caption{Procedural Automation}
  \label{fig:procedure}
\end{figure}

The advantage of procedural automation is that it resembles the
strategy of an analog designer, that means there are no surprises.
The disadvantage is, that initial effort must be spend in
creating the script.
Thus, the challenge for any procedural automation method is to make it as
easy as possible to capture the strategy.
When a 

\section{Expert~Design~Plan}

Our solution for procedural circuit design is called the
\glsfirst{edp} toolbox.
The diagram in Fig.~\ref{fig:edp-toolbox} illustrates the structure of the
tool.
On the right hand side is the expert \faUser \ and on the left hand side is the
design environment, i.e. \gls{virtuoso}.
During manual design, the expert would interact directly with the
design environment.

When the expert creates a script with the \gls{edp} toolbox, he interacts with 
a \gls{matlab}, \gls{octave} or \gls{python} environment (\textit{entry}).
The languages act as wrapper to the \textit{Core}.
The core can interchange designs the designs with the design environment
(\textit{exchange}).
When the expert starts to work with the \gls{edp} toolbox,
designs are read into the core.
When desired, the expert can trigger back-annotating designs to the 
design environment.

\begin{figure}[H]
  \centering
  \begin{tikzpicture}[scale=1.25]

    \useasboundingbox (-4.5,-2.0) rectangle (6,2.2);

    \draw[fill=gray!30!white,draw=black,rounded corners] (-1.6,2.1) rectangle (2.55,1.1); %4.15
    \node[] at (0.475,1.925) {Utilities};

    \node[] () at (0.475,0) {Core};
    \draw[semithick] (-1.45,-0.4) rectangle (2.4,0.4); %3.85

    \draw[-{Latex[round,scale=0.7]},shorten <=2pt,shorten >=2pt] (-2.0,-0.05) -- (-1.5,-0.05);
    \draw[-{Latex[round,scale=0.7]},shorten <=2pt,shorten >=2pt] (-1.5, 0.05) -- (-2.0, 0.05);

    \node[] () at (-2.2,0) {\faDatabase};
    \node[anchor=south,font=\scriptsize] () at (-2.2,0.1) {designs};

    \node[font=\scriptsize] () at (-3.6,0.0) {\begin{tabular}{c}design\\environment\end{tabular}};
    \draw[semithick] (-4.3,-0.4) rectangle (-2.9,0.4);

    \draw[{Latex[round,scale=0.7]}-,shorten <=3pt,shorten >=3pt] (-2.4,-0.05) -- (-2.9,-0.05);
    \draw[{Latex[round,scale=0.7]}-,shorten <=3pt,shorten >=3pt] (-2.9, 0.05) -- (-2.4, 0.05);

    \node[] () at (-1.2,1.5) {\faTable};
    \draw[-{Latex[round,scale=0.7]},shorten <=2pt,shorten >=2pt] (-1.25,1.4) -- (-1.25,0.4);
    \draw[-{Latex[round,scale=0.7]},shorten <=2pt,shorten >=2pt] (-1.15,0.4) -- (-1.15,1.4);
    \node[anchor=west,font=\scriptsize] () at (-1.1,1.5) {LUTs};

    \node[] () at (0.1,1.5) {\faGears};
    \node[anchor=west,font=\scriptsize] () at (0.2,1.5) {simulator};
    \draw[-{Latex[round,scale=0.7]},shorten <=2pt,shorten >=2pt] (0.05,1.4) -- (0.05,0.4);
    \draw[-{Latex[round,scale=0.7]},shorten <=2pt,shorten >=2pt] (0.15,0.4) -- (0.15,1.4);

    \node[] () at (1.75,1.5) {\faFileCodeO};
    \node[anchor=west,font=\scriptsize] () at (1.8,1.5) {PDK};
    \draw[-{Latex[round,scale=0.7]},shorten <=2pt,shorten >=2pt] (1.75,1.35) -- (1.75,0.4);

    \node[font=\scriptsize] () at (3.55,1.3) {\faFileTextO~EDP};

    \node[font=\scriptsize] () at (3.55,0.75) {\gls{matlab}};
    \node[font=\scriptsize] () at (3.55,0.0) {\gls{octave}};
    \node[font=\scriptsize] () at (3.55,-0.75) {\gls{python}};

    \draw[semithick] (3,-0.3)  rectangle (4.1,0.3);
    \draw[semithick] (3,0.45)  rectangle (4.1,1.05);
    \draw[semithick] (3,-0.45) rectangle (4.1,-1.05);

    \draw[-{Latex[round,scale=0.7]},shorten <=2pt,shorten >=2pt] (2.5,0.05) -- (3,0.05);
    \draw[-{Latex[round,scale=0.7]},shorten <=2pt,shorten >=2pt] (3,-0.05) -- (2.5,-0.05);

    \draw[-{Latex[round,scale=0.7]},shorten <=2pt,shorten >=2pt] (2.5,0.2) -- (3,0.8);
    \draw[-{Latex[round,scale=0.7]},shorten <=2pt,shorten >=2pt] (3,0.9) -- (2.5,0.3); 

    \draw[-{Latex[round,scale=0.7]},shorten <=2pt,shorten >=2pt] (2.5,-0.2) -- (3,-0.8);
    \draw[-{Latex[round,scale=0.7]},shorten <=2pt,shorten >=2pt] (3,-0.9) -- (2.5,-0.3); 

    \node[font=\scriptsize] () at (4.75,0) {\faUser};
    \node[font=\scriptsize,anchor=west] () at (4.8,0) {expert};

    \draw[-{Latex[round,scale=0.7]},shorten <=2pt,shorten >=2pt] (4.1,0.05) -- (4.55,0.05);
    \draw[-{Latex[round,scale=0.7]},shorten <=2pt,shorten >=2pt] (4.55,-0.05) -- (4.1,-0.05);

    \draw[-{Latex[round,scale=0.7]},shorten <=2pt,shorten >=2pt] (4.1,0.8) -- (4.55,0.1);
    \draw[-{Latex[round,scale=0.7]},shorten <=2pt,shorten >=2pt] (4.55,0.2) -- (4.1,0.9); 

    \draw[-{Latex[round,scale=0.7]},shorten <=2pt,shorten >=2pt] (4.1,-0.8) -- (4.65,-0.1);
    \draw[-{Latex[round,scale=0.7]},shorten <=2pt,shorten >=2pt] (4.65,-0.2) -- (4.1,-0.9);

    \draw [ decorate
          , decoration = {brace,raise=5pt, amplitude=5pt}
          ] (-2.25,-1.15) --  ( -4.3, -1.15)
          node [pos=0.5,black,anchor=north,below=10pt] {exchange};

    \draw [ decorate
          , decoration = {brace,raise=5pt, amplitude=5pt}
          ] (3.5,-1.15) --  (-2.15, -1.15)
          node[pos=0.5,black,anchor=north,below=10pt]{execution};

    \draw [ decorate
          , decoration = {brace,raise=5pt, amplitude=5pt}
          ] (5.75,-1.15) --  ( 3.6, -1.15)
          node[pos=0.5,black,anchor=north,below=10pt]{entry};
  \end{tikzpicture}
  \caption{Expert~Design~Plan Toolbox}
  \label{fig:edp-toolbox}
\end{figure}

The core itself is used for \textit{executing} an \gls{edp},
i.e. it accesses the \gls{pdk} and the simulator directly.