LATEXCOMPILER=pdflatex
GLOSSARYCOMPILER=makeglossaries

DOCUMENT=edp-man

all:*.tex
	./gitinfo
	${LATEXCOMPILER} -shell-escape -halt-on-error ${DOCUMENT}.tex
	${GLOSSARYCOMPILER} ${DOCUMENT}
	${LATEXCOMPILER} -shell-escape -halt-on-error ${DOCUMENT}.tex

draft:*.tex
	${LATEXCOMPILER} -draftmode -shell-escape -halt-on-error ${DOCUMENT}.tex
	${GLOSSARYCOMPILER} ${DOCUMENT}
	${LATEXCOMPILER} -shell-escape -halt-on-error ${DOCUMENT}.tex

.PHONY: clean

clean:
	rm -rf *.blg 
	rm -rf *.out 
	rm -rf *.bbl 
	rm -rf *.log
	rm -rf *.ind
	rm -rf *.ilg
	rm -rf *.lot
	rm -rf *.lof
	rm -rf *.ind
	rm -rf *.idx
	rm -rf *.aux
	rm -rf *.cut
	rm -rf *.toc
	rm -rf *.tdo
	rm -rf *.dpth
	rm -rf *.md5	
	rm -f  *.pdf
	rm -f  *.glg
	rm -f  *.glo
	rm -f  *.gls
	rm -f  *.ist
	rm -f  *.glsdefs
	rm -f  *.auxlock
	rm -f  *.acr
	rm -f  *.alg
	rm -f  *.slg
	rm -f  *.sls
	rm -f  *.acn
	rm -f  *.slo
	rm -f  *.cpt
