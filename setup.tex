\chapter{Installation and Setup}

When you want to utilize the \gls{edp} toolbox on your system, you must
perform several steps.
First you must install the tool itself and register it in 
\gls{virtuoso} (section~\ref{subsec:setup:tool}).

Secondly, you must install the \gls{edp} specific \gls{pdk} setups 
(section~\ref{subsec:setup:tech}).
There is also a \gls{cds} specific technology setup that must be 
added.
When there is no setup for your \gls{pdk} available, you 
must create it as described in chapter~\ref{chap:techfile}.
Optionally, you can add \gls{predict} \glspl{lut} tables for
sizing with the $\nicefrac{g_{\mathrm{m}}}{I_{\mathrm{D}}}$ method.

\section{Requirements}\label{subsec:setup:requirements}

Before you install the \gls{edp} toolbox, please check whether all
relevant dependencies are available on your machine.

\medskip

Check first with
\begin{lstlisting}[language=bash, numbers=none, xleftmargin=1cm, frame=none]
git version
\end{lstlisting}
whether \texttt{git} is available.
As a next step, a \gls{jvm}%
\footnote{You can have an \textit{OpenJDK} \gls{jvm} 
(\href{https://openjdk.org/}{\texttt{https://openjdk.org/}}) or an 
\textit{Oracle} \gls{jvm}.}
must be installed on your machine.
The command 
\begin{lstlisting}[language=bash, numbers=none, xleftmargin=1cm, frame=none]
java -version
\end{lstlisting}
will return the version number. 
Make sure that you've installed \texttt{1.8.0} (Java-8).
You may point your setup to the \gls{jvm} with the environment variable
explained in appendix \ref{chapter:ennvars}.

\medskip

Check with 
\begin{lstlisting}[language=bash, numbers=none, xleftmargin=1cm, frame=none]
virtuoso -W
\end{lstlisting}
that the \gls{virtuoso} version in use is at least \texttt{6.1.8} and
with 
\begin{lstlisting}[language=bash, numbers=none, xleftmargin=1cm, frame=none]
spectre -W
\end{lstlisting}
that the \gls{spectre} version in use is at least \texttt{19.0}.
When \gls{python} is used for \gls{edp} coding, check also with
\begin{lstlisting}[language=bash, numbers=none, xleftmargin=1cm, frame=none]
python3 -V
\end{lstlisting}
that at least \texttt{3.1} or newer is available.

\section{Tool}\label{subsec:setup:tool}

The directory structure of the tool is shown in Fig. \ref{fig:di-struct}.
The individual sub-directories are distributed separately.

\begin{figure}[H]
\centering
\framebox[0.55\textwidth]{%
\begin{minipage}{0.55\textwidth}
  \dirtree{%
  .1 edp.
  .2 bin.  
  .2 py. 
  .2 mat. 
  .2 core.  
  .3 jar.
  .4 edp-core.jar.
  .3 init.
  .4 init.ile.
  .4 init.m.
  }
\end{minipage}
}
\caption{Directory Structure of the EDP Toolbox}
\label{fig:di-struct}
\end{figure}

First, create the top-directory with
\begin{lstlisting}[language=bash, numbers=none, xleftmargin=1cm, frame=none]
mkdir edp
\end{lstlisting}
and navigate in the diretory
\begin{lstlisting}[language=bash, numbers=none, xleftmargin=1cm, frame=none]
cd edp
\end{lstlisting}
The directories \texttt{py}, \texttt{mat} and \texttt{bin}
are publicly available.
Clone the directories with
\begin{lstlisting}[language=bash, numbers=none, xleftmargin=1cm, frame=none]
git clone git@gitlab-forschung.reutlingen-university.de:eda/edp/edp-bin.git bin
git clone git@gitlab-forschung.reutlingen-university.de:eda/edp/edp-py.git py
git clone git@gitlab-forschung.reutlingen-university.de:eda/edp/edp-mat.git mat
\end{lstlisting}
When there are any updates, you can simply update them with
\begin{lstlisting}[language=bash, numbers=none, xleftmargin=1cm, frame=none]
git pull
\end{lstlisting}

The \texttt{core} is shipped separately as a single container 
\texttt{edp-core-XYZ.tar.gz}.
The placeholders XYZ corresponds to the version.
Copy the file in the \texttt{edp} directory and 
unpack the container with
\begin{lstlisting}[language=bash, numbers=none, xleftmargin=1cm, frame=none]
tar -zvxf edp-core-XYZ.tar.gz
\end{lstlisting}
When unpacking is finished, the new directory \texttt{core}
is created.
Afterwards you must point the environment variables \texttt{ED\_EDP\_HOME} and 
\texttt{PATH} to this directory.
Adjust the path to the installation directory in the command template
below  accordingly.
\begin{lstlisting}[language=bash, numbers=none, xleftmargin=1cm, frame=none]
export ED_EDP_HOME=<PATH-TO-DIR>/edp
export PATH=$PATH:$ED_EDP_HOME/bin
\end{lstlisting}

\medskip

Secondly, you must load some \gls{skill} code in \gls{virtuoso}.
Execute the command
\begin{lstlisting}[language=bash, numbers=none, xleftmargin=1cm, frame=none]
load(strcat(getShellEnvVar("ED_EDP_HOME") "/core/init/init.ile"))
\end{lstlisting}
in the \gls{ciw} or add it to your \texttt{.cdsinit}.
In addition, you can add a custom menu to the \gls{ciw}. 
Execute the command
\begin{lstlisting}[language=bash, numbers=none, xleftmargin=1cm, frame=none]
EDedpInitMenu()
\end{lstlisting}
to add this menu.
When the menu is loaded successfully, a new menu item (\textbf{EDP}) 
appears in the menu bar of the \gls{ciw} (Fig.~\ref{fig:edp-in-ciw}).

\begin{figure}[H]
  \centering
   \begin{tikzpicture}
   \node[] () at (0,0) 
       {\includegraphics[scale=0.4]{./figures/edp-in-ciw.png}};
      %\draw[step=1mm,blue,very thin] (-6,-2) grid (6,2);
      %\draw[step=1cm,red,thick] (-6,-2) grid (6,2);
      \node[BlueArrow, rotate=-90] at (-2.55,1.5){};  
   \end{tikzpicture}
  \caption{EDP~menu in the \gls{ciw}}
  \label{fig:edp-in-ciw}
\end{figure}

\section{Technology}\label{subsec:setup:tech}

An \gls{edp} technology setup is shipped as a single container .
Unpack the container with
\begin{lstlisting}[language=bash, numbers=none, xleftmargin=1cm, frame=none]
tar -zvxf edp-pdk-node.tar.gz
\end{lstlisting}

The unpacked directory contains a technology file 
\texttt{edp-pdk-node.xml} encoded as \gls{xml} 
(conf. chaper \ref{chap:techfile}).

\section{Python}\label{subsec:setup:python}

When \gls{edp} development is performed with \gls{python}, we recommend to
use \gls{conda}.
After installation%
\footnote{\url{https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html}}
, create a new environment with
\begin{lstlisting}[language=bash, numbers=none, xleftmargin=1cm, frame=none]
conda create --name edp-wk-tech
\end{lstlisting}
Then, you can enter/activate the environment with
\begin{lstlisting}[language=bash, numbers=none, xleftmargin=1cm, frame=none]
conda activate edp-wk-tech
\end{lstlisting}
You can leave the environment with the command
\begin{lstlisting}[language=bash, numbers=none, xleftmargin=1cm, frame=none]
conda deactivate
\end{lstlisting}

\medskip

In order to work with the \gls{edp} in the above shown environment, you
must install the corresponding package with \gls{pip}
\begin{lstlisting}[language=bash, numbers=none, xleftmargin=1cm, frame=none]
pip install $ED_EDP_HOME/py
\end{lstlisting}
