%% WATERMARK
\usepackage{helvet}

%% VARIABLES AND MACROS
\usepackage[local]{gitinfo2}
\def \gitRepository   {https://gitlab-forschung.reutlingen-university.de/eda/edp/edp-man}
\def \title           {Expert~Design~Plan~Toolbox}
\def \subtitle        {User~Guide}
\def \university      {Reutlingen~University}
\def \univUrl         {www.reutlingen-university.de}
\def \uniStreet       {Alteburgstraße~150}
\def \uniCity         {72762 Reutlingen}
\def \institution     {Electronics~\&~Drives}
\def \group           {Electronic~Design~Automation~Research~Group}
\def \instUrl         {www.electronics-and-drives.de}
\def \instStreet      {Oferdinger~Straße 50}
\def \instCity        {72768~Rommelsbach}
\def \Year            {2023}
\def \auth            {Matthias Schweikardt, Jürgen Scheible}
\def \mail            {\{matthias.schweikardt, juergen.scheible\}@reutlingen-university.de}
\def \matlab  {2023}
%\def \python  {2023}

%% COLORS

\usepackage{xcolor}
  \definecolor{ed}{HTML}{00387B}
  \definecolor{footcolor}{RGB}{112,112,115}

\usepackage[a4paper,left=2cm,right=2cm, top=2cm, bottom=2.5cm]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[affil-it]{authblk}

\usepackage{tikz}
  \usetikzlibrary{shapes.arrows}
  \usetikzlibrary{arrows}
  \usetikzlibrary{external}
  \usetikzlibrary{calc}
  \usetikzlibrary{positioning}
  \usetikzlibrary{decorations}
  \usetikzlibrary{decorations.pathreplacing}
  \usetikzlibrary{decorations.markings}  
  \usetikzlibrary{angles}
  \usetikzlibrary{arrows.meta}
  \usetikzlibrary{shapes}
  \usetikzlibrary{shapes.symbols}
  \usetikzlibrary{automata}
  \usetikzlibrary{patterns}
  \usetikzlibrary{decorations.text}
  

\usepackage{circuitikz}
  \ctikzset{bipoles/cisourceam/height=0.5}
  \ctikzset{bipoles/cisourceam/width=0.5}
  \ctikzset{bipoles/resistor/width=0.5}
  \ctikzset{bipoles/resistor/height=0.2}
  \ctikzset{bipoles/capacitor/width=0.1}
  \ctikzset{bipoles/capacitor/height=0.4}
\usepackage{pgfplots}

\usepackage{pdflscape}
\usepackage{comment}
\usepackage{float}
\usepackage{siunitx}
\usepackage{textcomp}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{nicefrac}
\usepackage{todonotes}
\usepackage{dirtree}

\usepackage[hang,flushmargin]{footmisc}
\usepackage[ hyperfootnotes=false
           , colorlinks=true
           , linkcolor=ed
           , citecolor=black
           , urlcolor=black
           , filecolor=black
           , runcolor=black
           ]{hyperref}
\hypersetup{
    pdftitle={\title},
    pdfauthor={Reutlingen University, Electronics \& Drives},
    pdfkeywords={\gitHash}
}

\usepackage{footnotebackref}

\usepackage{listings}
\lstset{ basicstyle=\ttfamily\footnotesize
       , escapeinside=||
       , xleftmargin=.05\textwidth
       , float
       , floatplacement=H
       , upquote=true
       }

\lstdefinestyle{xml}{ basicstyle=\tt\small
                    , frame=single
                    , language=xml
                    , escapeinside={(*@}{@*)}
                    , columns=fullflexible
                    , linewidth=0.9\textwidth
                    , xleftmargin=.05\textwidth
                    }

\lstdefinestyle{java}{ basicstyle=\tt\small
                     , escapeinside={(*@}{@*)}
                     , columns=fullflexible
                     , linewidth=0.9\textwidth
                     , xleftmargin=.05\textwidth
                     , numbers=left
                     , frame=l
                     }

\lstdefinestyle{mat}{ basicstyle=\tt\small
                    , escapeinside={(*@}{@*)}
                    , columns=fullflexible
                    , linewidth=0.9\textwidth
                    , xleftmargin=.1\textwidth
                    , numbers=left
                    , frame=l
                    , upquote=true
                    }

\usepackage{lmodern}
\usepackage{graphicx}
\usepackage{fontawesome}
\usepackage{enumitem}


\setlength\parindent{0pt}

\usepackage{background}
\usepackage{tikzpagenodes}

% add Copyright notice at every page (left & right)
\backgroundsetup{
    angle=0
  , opacity=1
  , scale=1
  , contents={\begin{tikzpicture}[remember picture,overlay, scale=3]
                \fontsize{25}{45}\fontfamily{phv}\selectfont
                \node[ text=lightgray
                     , rotate=90
                     , anchor=north
                     ] at ($(current page.west)+(0.2,0)$)
                     {\textcopyright~\the\year, %
                       \href{\univUrl}{\color{lightgray}\university},
                       \href{\instUrl}{\color{lightgray}\institution}
                     };
                \node[ text=lightgray
                     , rotate=-90
                     , anchor=north
                     ] at ($(current page.east)-(0.2,0)$)
                     {\textcopyright~\the\year, %
                       \href{\univUrl}{\color{lightgray}\university},
                       \href{\instUrl}{\color{lightgray}\institution}
                     };          
                \end{tikzpicture}
              }
}

%% GLOSSARY
\usepackage[]{glossaries}

\makeglossaries

\include{glossaryentries}


%% HEADER AND FOOTER
\usepackage{lastpage}
\usepackage{fancyhdr}
  \setlength{\headheight}{16pt}

\definecolor{footcolor}{RGB}{112,112,115}

\newcommand{\footfont}[1]{
  {\fontfamily{phv}\selectfont 
    \scriptsize
    \textcolor{footcolor}{#1}
  }
}

\makeatletter
\renewcommand\@makefntext[1]{\leftskip=1em\hskip-1em\@makefnmark#1}
\makeatother

\setlength{\headsep}{11pt}
\setlength{\footskip}{2cm}

%footline
\fancypagestyle{plain}
{
  \fancyhf{}
  \renewcommand{\headrulewidth}{0.5pt}
  \renewcommand{\footrulewidth}{0.0pt}

  \fancyhead[C]{\title}

  \fancyfoot[C]{
    \begin{tikzpicture}
      \node[inner sep=0pt] (silouette) at (0,0) 
        {\includegraphics[ keepaspectratio
                         , width=\textwidth
                         ]{./figures/coorp/silouette.pdf}};
      \node[anchor= north west,inner sep=0pt,inner xsep=0pt,inner ysep=3pt] 
        (symbol) at (silouette.south west) 
        {\includegraphics[ keepaspectratio
                         , height=0.55cm
                         ]{./figures/coorp/edsymbol.pdf}};
      \node[anchor= west,inner sep=3pt] () at ($(symbol.east)+(-0.2, 0.12)$) 
        {\footfont{
          \href{\univUrl}{\color{footcolor}\university}, 
          \href{\instUrl}{\color{footcolor}\institution}, \title 
          , Version:~\gitRel, \href{\gitRepository/-/tree/\gitHash}
                             {\color{footcolor}\gitAbbrevHash}}};
      \node[anchor= west,inner sep=3pt] () at ($(symbol.east)+(-0.2,-0.17)$)
        {\footfont{Postal Address: \uniStreet, \uniCity \quad
                   Visitor Address: \instStreet, \instCity}};
      \node[anchor= east,inner xsep=4pt,inner ysep=0pt] 
        (pagenum) at (silouette.east|-symbol)
        {{\fontfamily{phv}\selectfont \thepage/\pageref*{LastPage}}};
    \end{tikzpicture}
  }
}
\pagestyle{plain}

\tikzstyle{BlueArrow}=[ single arrow
                      , draw
                      , single arrow head extend=0.2em
                      , inner ysep=0.2em
                      , transform shape
                      , line width=0.05em
                      , top color=blue!70
                      , bottom color=blue!70
                      ]