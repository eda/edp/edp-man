\chapter{Schematic~Generation}

\tikzset{
  block/.pic = {
    \useasboundingbox (-0.8,-1) rectangle (0.8,1);

    \draw[thick] (-0.8,-1) rectangle (0.8,1);
    \draw[thick] (-0.8,-1) rectangle (0,0);
    \filldraw[black] (0.4, 0.5) circle (1.5pt);
  }
}

Schematic generation refers to the the "physical" task of drawing of the 
schematic, i.e., placing instances (transistors), drawing wires and pins and
so on.
The \gls{edp} toolbox provides several commands to draw a schematic
within a script.
Generating a schematic with a \gls{edp} is only useful when
\begin{itemize}
  \item the schematic has a high regularity (matrix-structure) or
  \item the schematic must be instantiated multiple times (high reuseability) or
  \item the user is keen to programming.
\end{itemize}

Generating a schematic with code is a common task in other domains,
e.g., usepackage \textit{CircuiTikZ}~\cite{circuitikz} in \LaTeX. 
Some of the mechanisms in this toolbox are derived from this usepackage.
This document gives a tutorial for the \textit{Schematic~Generation} features
within the \gls{edp} toolbox.
More detailed information can be retrieved from 
the \gls{edp} code documentation.

\section{Naming~Conventions}

In a first step, we will denote the different parts of a schematic.
For sure, everyone that is familiar with circuit design is well aware 
of the different elements in a schematic, but for completeness we
will explain them here again and provide the names they have in the
\gls{edp}.

\begin{itemize}
\item The instances in a \texttt{SchematicGenerator} are of 
  type \texttt{GeneratedInstance}, because they are used in a generator.
\item Wires connect the terminals of the instances and form so called
  nets.
  Designers can give these nets a name, they are annotated as labels on
  the wire segments.
  The instances in a \texttt{SchematicGenerator} are of 
  type \texttt{GeneratedWire}.
\end{itemize}

\section{Startup}

A new schematic generator is created in the database.

\ifdefined\matlab
\begin{lstlisting}[style=mat,numbers=none,frame=none,xleftmargin=.03\textwidth]
gen = db.createSchematicGenerator('edp_dsgn', 'diffamp', 'schematic');
\end{lstlisting}
\fi
\ifdefined\python
\begin{lstlisting}[style=mat,numbers=none,frame=none,xleftmargin=.03\textwidth]
gen = db.createSchematicGenerator('edp_dsgn', 'diffamp', 'schematic');
\end{lstlisting}
\fi
You must provide the destination (library name, cell name, view name)
of the resulting schematic as parameters.
The resulting variable \texttt{gen} is a handle to the schematic generator.
Using this variable you can add instances, wires and pins.
Before we can instantiate a new symbol, we need a reference to an existing
symbol that we want to instantiate this is called \texttt{SymbolMaster}.
Provide the library name, cell name, view name%
\footnote{The view name of a symbol is almost always \texttt{symbol}.}
to access the \texttt{SymbolMaster}.
\ifdefined\matlab
\begin{lstlisting}[style=mat,numbers=none,frame=none,xleftmargin=.03\textwidth]
master = gen.getSymbolMaster('PRIMLIB', 'diffamp', 'schematic');
\end{lstlisting}
\fi
\ifdefined\python
\begin{lstlisting}[style=mat,numbers=none,frame=none,xleftmargin=.03\textwidth]
master = gen.getSymbolMaster('PRIMLIB', 'diffamp', 'schematic');
\end{lstlisting}
\fi
An anchor is a point where we can grab the symbol.
A typical anchor of a symbol is one of its terminal, e.g., 
\texttt{D}, \texttt{G}, \texttt{S} or \texttt{B} of a MOS transistor.

\medskip

Wires are used to connect instances and pins.

\ifdefined\matlab
\begin{lstlisting}[style=mat,numbers=none,frame=none,xleftmargin=.03\textwidth]
wire = gen.wire(a, b)
\end{lstlisting}
\fi
\ifdefined\python
\begin{lstlisting}[style=mat,numbers=none,frame=none,xleftmargin=.03\textwidth]
wire = gen.wire([1, 1], [6, 4])
\end{lstlisting}
\fi

\begin{figure}[H]
  \centering
  \input{./tikz/routing-style-direct.tex}
  \caption{Routing style \textit{direct} \texttt{--}}
  \label{fig:routing-style:--}
\end{figure}

The route can be spitted up in a horizontal and vertical connection
by providing the additional keyword parameter \texttt{style} with the
value \texttt{'-|'} (Fig.~\ref{fig:routing-style:-|}):
\ifdefined\matlab
\begin{lstlisting}[style=mat,numbers=none,frame=none,xleftmargin=.03\textwidth]
wire = gen.wire([1, 1], [6, 4], 'style', '-|')
\end{lstlisting}
\fi
\ifdefined\python
\begin{lstlisting}[style=mat,numbers=none,frame=none,xleftmargin=.03\textwidth]
wire = gen.wire([1, 1], [6, 4], 'style', '-|')
\end{lstlisting}
\fi

\begin{figure}[H]
  \centering
  \input{./tikz/routing-style-horizontal-vertical.tex}
  \caption{Routing style \textit{horizontal-vertical} \texttt{-|}}
  \label{fig:routing-style:-|}
\end{figure}

The opposite, i.e., first vertical an then horizontal is specified 
with the parameter \texttt{'|-'} (Fig.~\ref{fig:routing-style:|-}):
\ifdefined\matlab
\begin{lstlisting}[style=mat,numbers=none,frame=none,xleftmargin=.03\textwidth]
wire = gen.wire([1, 1], [6, 4], 'style', '|-')
\end{lstlisting}
\fi
\ifdefined\python
\begin{lstlisting}[style=mat,numbers=none,frame=none,xleftmargin=.03\textwidth]
wire = gen.wire([1, 1], [6, 4], 'style', '|-')
\end{lstlisting}
\fi
\begin{figure}[H]
  \centering
  \input{./tikz/routing-style-vertical-horizontal.tex}
  \caption{Routing style \textit{vertical-horizontal} \texttt{|-}}
  \label{fig:routing-style:|-}
\end{figure}

\begin{figure}[H]
  \centering
  \input{./tikz/routing-style-vertical.tex}
  \caption{Routing style \textit{vertical} \texttt{|}}
  \label{fig:routing-style:|}
\end{figure}

\begin{figure}[H]
  \centering
  \input{./tikz/routing-style-horizontal.tex}
  \caption{Routing style \textit{horizontal} \texttt{-}}
  \label{fig:routing-style:-}
\end{figure}